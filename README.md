### endpoints:  

 * detect gender:  
  `$host:$port/v1/gender/name/$names?variant=$variantType&version=$variantVersion`  
 * show names list:  
  `$host:$port/v1/gender/list/$gender` (not implemented)  
 
 where:  
`$host` is host of the app    
`$port` is port of the app   
`$names` are names, separated by `%20` or `+` (space)  
`$variantType` is variant type of the gender detection algorithm  
`$variantVersion` is variant version of the gender detection algorithm(use `%2E` for dot)   
`$gender` is  `female` or `male` 
   
### example server start:  

```
java -jar gender-detector-core-1.0.2.jar \
    --spring.profiles.active=local \
    --names.females.path=C:/examplePath/females.txt \
    --names.males.path=C:/examplePath/males.txt
```
  
 
### example client usage:  

`curl -x GET http://localhost:8080/v1/gender/names/Jan%20Maria%20rokita?variant=MAJOR&version=1%2E0`  
`curl -x GET http://localhost:8080/v1/gender/names/Anna%20Zbigniew%20Gertruda?variant=FIRST&version=1%2E0`  
`curl -x GET http://localhost:8080/v1/gender/list/female` (not implemented)

#### see:  
https://en.wikipedia.org/wiki/Percent-encoding  