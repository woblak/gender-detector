package com.woblak.detector.gender.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Optional;

@RequiredArgsConstructor
@Getter
public enum Gender {

    FEMALE("FEMALE", -1),
    MALE("MALE", 1),
    INCONCLUSIVE("INCONCLUSIVE", 0);

    private final String value;
    private final int id;
}