package com.woblak.detector.gender.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("local")
@Configuration
public class RepoPathsConfig {

    @Bean
    public String namesFemalesPath(@Value("${names.females.path}") String pathStr) {
        return pathStr;
    }

    @Bean
    public String namesMalesPath(@Value("${names.males.path}") String pathStr) {
        return pathStr;
    }
}
