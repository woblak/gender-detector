package com.woblak.detector.gender.core.controller.v1;

import com.woblak.detector.gender.api.Gender;
import com.woblak.detector.gender.core.exceptions.InvalidNameForGenderDetectorException;
import com.woblak.detector.gender.core.exceptions.InvalidVariantTypeForGenderDetectorException;
import com.woblak.detector.gender.core.exceptions.NotFoundGenderDetectorException;
import com.woblak.detector.gender.core.service.detector.GenderDetectorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/gender")
@RequiredArgsConstructor
@Slf4j
public class GenderDetectorController {

    private final GenderDetectorService genderDetectorService;

    @GetMapping("names/{names}")
    public ResponseEntity<String> detectGender(
            @PathVariable("names") String names,
            @RequestParam("variant") String variantType,
            @RequestParam("version") String variantVersion
    ) {
        String body;
        HttpStatus status;

        try {
            Gender gender = genderDetectorService.detectGender(names, variantType, variantVersion);
            body = gender.getValue();
            status = HttpStatus.OK;
        } catch (InvalidNameForGenderDetectorException
                | InvalidVariantTypeForGenderDetectorException
                | NotFoundGenderDetectorException e) {
            log.error(e.getMessage(), e);
            body = e.getMessage();
            status = HttpStatus.BAD_REQUEST;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            body = e.getMessage();
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<>(body, status);
    }

}
