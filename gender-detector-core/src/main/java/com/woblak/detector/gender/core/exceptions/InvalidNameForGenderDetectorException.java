package com.woblak.detector.gender.core.exceptions;

public class InvalidNameForGenderDetectorException extends IllegalArgumentException {

    public InvalidNameForGenderDetectorException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidNameForGenderDetectorException(String message) {
        super(message);
    }
}
