package com.woblak.detector.gender.core.exceptions;

public class InvalidVariantTypeForGenderDetectorException extends IllegalArgumentException {

    public InvalidVariantTypeForGenderDetectorException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidVariantTypeForGenderDetectorException(String message) {
        super(message);
    }
}
