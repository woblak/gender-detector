package com.woblak.detector.gender.core.exceptions;

import com.woblak.detector.gender.core.service.detector.GenderDetectorVariant;

public class NotFoundGenderDetectorException extends UnsupportedOperationException {

    public NotFoundGenderDetectorException(GenderDetectorVariant variant) {
        super("Not found gender detector for given variant: " + variant.toString());
    }
}
