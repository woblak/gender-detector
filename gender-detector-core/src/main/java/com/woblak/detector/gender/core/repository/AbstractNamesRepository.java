package com.woblak.detector.gender.core.repository;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Slf4j
public abstract class AbstractNamesRepository implements NamesRepository {

    protected final Path path;

    protected AbstractNamesRepository(String pathName) {
        this.path = Paths.get(pathName);
    }

    @Override
    public boolean exist(String name) {
        try (Stream<String> lines = Files.lines(path)) {
            return lines.anyMatch(line -> line.equalsIgnoreCase(name));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new UnsupportedOperationException(e.getMessage(), e);
        }
    }
}
