package com.woblak.detector.gender.core.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class FemalesNamesRepository extends AbstractNamesRepository {

    protected FemalesNamesRepository(@Qualifier("namesFemalesPath") String pathName) {
        super(pathName);
    }
}
