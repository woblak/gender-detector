package com.woblak.detector.gender.core.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class MalesNamesRepository extends AbstractNamesRepository {

    protected MalesNamesRepository(@Qualifier("namesMalesPath") String pathName) {
        super(pathName);
    }
}
