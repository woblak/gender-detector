package com.woblak.detector.gender.core.repository;

public interface NamesRepository {

    boolean exist(String name);
}
