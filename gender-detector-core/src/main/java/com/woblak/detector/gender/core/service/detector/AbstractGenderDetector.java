package com.woblak.detector.gender.core.service.detector;

import com.woblak.detector.gender.api.Gender;
import com.woblak.detector.gender.core.repository.FemalesNamesRepository;
import com.woblak.detector.gender.core.repository.MalesNamesRepository;
import com.woblak.detector.gender.core.util.UtilConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public abstract class AbstractGenderDetector implements GenderDetector {

    private final UtilConverter utilConverter;
    private final FemalesNamesRepository femalesNamesRepository;
    private final MalesNamesRepository malesNamesRepository;

    @Override
    public boolean test(GenderDetectorVariant variant) {
        return getVariantType() == variant.getType() && getVariantVersion().equals(variant.getVersion());
    }

    @Override
    public Gender apply(String names) {
        return Gender.INCONCLUSIVE;
    }

    protected abstract GenderDetectorVariant.Type getVariantType();

    protected abstract String getVariantVersion();

    protected List<String> namesToNamesList(String names) {
        return utilConverter.strToStrList(names);
    }

    protected Gender nameToGender(String name) {
        Gender result = Gender.INCONCLUSIVE;

        boolean isFemale = isFemale(name);
        boolean isMale = isMale(name);

        if (isFemale && !isMale) {
            result = Gender.FEMALE;
        }
        else if (!isFemale && isMale) {
            result = Gender.MALE;
        }

        return result;
    }

    protected boolean isFemale(String name) {
        return femalesNamesRepository.exist(name);
    }

    protected boolean isMale(String name) {
        return malesNamesRepository.exist(name);
    }
}
