package com.woblak.detector.gender.core.service.detector;

import com.woblak.detector.gender.api.Gender;

public interface GenderDetector {

    boolean test(GenderDetectorVariant variant);

    Gender apply(String names);
}
