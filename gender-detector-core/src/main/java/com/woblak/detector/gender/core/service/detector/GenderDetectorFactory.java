package com.woblak.detector.gender.core.service.detector;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;

@Component
@RequiredArgsConstructor
class GenderDetectorFactory {

    private final Set<GenderDetector> genderDetectors;

    public Optional<GenderDetector> findGenderDetector(GenderDetectorVariant variant) {
        return genderDetectors.stream()
                .filter(genderDetector -> genderDetector.test(variant))
                .findFirst();
    }
}
