package com.woblak.detector.gender.core.service.detector;

import com.woblak.detector.gender.api.Gender;
import com.woblak.detector.gender.core.repository.FemalesNamesRepository;
import com.woblak.detector.gender.core.repository.MalesNamesRepository;
import com.woblak.detector.gender.core.util.UtilConverter;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Getter
/**
 * <p>Recognize gender only by first name.</p>
 */
public class GenderDetectorFirstV1 extends AbstractGenderDetector {

    public static final GenderDetectorVariant.Type VARIANT_TYPE = GenderDetectorVariant.Type.FIRST;
    public static final String VARIANT_VERSION = "1.0";

    public GenderDetectorFirstV1(
            UtilConverter utilConverter,
            FemalesNamesRepository femalesNamesRepository,
            MalesNamesRepository malesNamesRepository
    ) {
        super(utilConverter, femalesNamesRepository, malesNamesRepository);
    }

    @Override
    public Gender apply(String names) {
        List<String> namesList = super.namesToNamesList(names);
        return namesList.stream().findFirst().map(this::nameToGender)
                .orElseThrow(UnsupportedOperationException::new);
    }

    @Override
    protected GenderDetectorVariant.Type getVariantType() {
        return VARIANT_TYPE;
    }

    @Override
    protected String getVariantVersion() {
        return VARIANT_VERSION;
    }

}
