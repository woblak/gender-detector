package com.woblak.detector.gender.core.service.detector;

import com.woblak.detector.gender.api.Gender;
import com.woblak.detector.gender.core.repository.FemalesNamesRepository;
import com.woblak.detector.gender.core.repository.MalesNamesRepository;
import com.woblak.detector.gender.core.util.UtilConverter;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Getter
/**
 * <p>Recognize gender by all names. Algorithm votes for the gender for each name. The most popular wins.</p>
 */
public class GenderDetectorMajorV1 extends AbstractGenderDetector {

    public static final GenderDetectorVariant.Type VARIANT_TYPE = GenderDetectorVariant.Type.MAJOR;
    public static final String VARIANT_VERSION = "1.0";

    public GenderDetectorMajorV1(
            UtilConverter utilConverter,
            FemalesNamesRepository femalesNamesRepository,
            MalesNamesRepository malesNamesRepository
    ) {
        super(utilConverter, femalesNamesRepository, malesNamesRepository);
    }

    @Override
    public Gender apply(String names) {
        List<String> namesList = super.namesToNamesList(names);
        int genderPoints = calcGenderPoints(namesList);
        return evaluateGenderPoints(genderPoints);
    }

    @Override
    protected GenderDetectorVariant.Type getVariantType() {
        return VARIANT_TYPE;
    }

    @Override
    protected String getVariantVersion() {
        return VARIANT_VERSION;
    }

    private int calcGenderPoints(List<String> namesList) {
        return namesList.stream()
                .map(this::nameToGender)
                .map(Gender::getId)
                .reduce(Integer::sum)
                .orElse(0);
    }

    private Gender evaluateGenderPoints(int genderPoints) {
        Gender result = Gender.INCONCLUSIVE;

        if (genderPoints > 0) {
            result = Gender.MALE;
        }
        else if (genderPoints < 0) {
            result = Gender.FEMALE;
        }

        return result;
    }
}
