package com.woblak.detector.gender.core.service.detector;

import com.woblak.detector.gender.api.Gender;
import com.woblak.detector.gender.core.exceptions.InvalidNameForGenderDetectorException;
import com.woblak.detector.gender.core.exceptions.InvalidVariantTypeForGenderDetectorException;
import com.woblak.detector.gender.core.exceptions.NotFoundGenderDetectorException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GenderDetectorService {

    private final GenderDetectorFactory genderDetectorFactory;

    public Gender detectGender(String names, String variantType, String variantVersion) {
        if (names == null || names.isEmpty()) {
            throw new InvalidNameForGenderDetectorException(names);
        }
        GenderDetector genderDetector = getGenderDetector(variantType, variantVersion);
        return genderDetector.apply(names);
    }

    private GenderDetector getGenderDetector(String variantType, String variantVersion) {
        var type = GenderDetectorVariant.Type.findByValue(variantType)
                .orElseThrow(() -> new InvalidVariantTypeForGenderDetectorException(variantType));
        var variant = GenderDetectorVariant.builder()
                .type(type)
                .version(variantVersion)
                .build();
        return genderDetectorFactory.findGenderDetector(variant)
                .orElseThrow(() -> new NotFoundGenderDetectorException(variant));
    }
}
