package com.woblak.detector.gender.core.service.detector;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.Arrays;
import java.util.Optional;

@Value
@Builder
public class GenderDetectorVariant {

    Type type;
    String version;

    @RequiredArgsConstructor
    @Getter
    public enum Type {

        FIRST("FIRST"),
        MAJOR("MAJOR");

        private final String value;

        public static Optional<Type> findByValue(String wantedValue) {
            return Arrays.stream(Type.values())
                    .filter(gender -> gender.value.equalsIgnoreCase(wantedValue))
                    .findFirst();
        }
    }
}
