package com.woblak.detector.gender.core.util;

import org.springframework.stereotype.Component;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class DefaultUtilConverter implements UtilConverter {

    public List<String> strToStrList(String names) {
        if (names == null || names.isEmpty()) {
            return Collections.emptyList();
        }
        String namesDecoded = URLDecoder.decode(names, StandardCharsets.UTF_8);
        return Arrays.asList(namesDecoded.split(" "));
    }
}
