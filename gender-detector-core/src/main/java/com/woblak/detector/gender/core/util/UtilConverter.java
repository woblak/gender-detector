package com.woblak.detector.gender.core.util;

import java.util.List;

public interface UtilConverter {

    List<String> strToStrList(String names);
}
