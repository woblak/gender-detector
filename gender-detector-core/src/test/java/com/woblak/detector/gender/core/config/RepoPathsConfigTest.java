package com.woblak.detector.gender.core.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@TestConfiguration
public class RepoPathsConfigTest {

    @Bean
    @Primary
    public String namesFemalesPath() {
        return getPathNameToTestResources("females.txt");
    }

    @Bean
    @Primary
    public String namesMalesPath() {
        return getPathNameToTestResources("males.txt");
    }

    private String getPathNameToTestResources(String fileName) {
        return this.getClass().getClassLoader().getResource(fileName).getPath().substring(1);
    }
}
