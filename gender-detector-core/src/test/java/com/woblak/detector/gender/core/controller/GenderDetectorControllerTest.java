package com.woblak.detector.gender.core.controller;

import com.woblak.detector.gender.api.Gender;
import com.woblak.detector.gender.core.config.RepoPathsConfigTest;
import com.woblak.detector.gender.core.service.detector.GenderDetectorFirstV1;
import com.woblak.detector.gender.core.service.detector.GenderDetectorMajorV1;
import com.woblak.detector.gender.core.service.detector.GenderDetectorVariant;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(RepoPathsConfigTest.class)
final class GenderDetectorControllerTest {

    private final static GenderDetectorVariant FIRST_GENDER_DETECTOR = GenderDetectorVariant.builder()
            .type(GenderDetectorFirstV1.VARIANT_TYPE)
            .version(GenderDetectorFirstV1.VARIANT_VERSION)
            .build();

    private final static GenderDetectorVariant MAJOR_GENDER_DETECTOR = GenderDetectorVariant.builder()
            .type(GenderDetectorMajorV1.VARIANT_TYPE)
            .version(GenderDetectorMajorV1.VARIANT_VERSION)
            .build();

    private final int serverPort;

    private final HttpClient httpClient;

    public GenderDetectorControllerTest(@LocalServerPort int serverPort) {
        this.serverPort = serverPort;
        this.httpClient = HttpClient.newHttpClient();
    }

    @Test
    void shouldGetFemaleForFirstFemaleNameWithVariantFirst() throws IOException, InterruptedException {
        testGenderDetectorEndpoint(FIRST_GENDER_DETECTOR, "Maria Jan Rokita", Gender.FEMALE);
    }

    @Test
    void shouldGetMaleForFirstMaleNameWithVariantFirst() throws IOException, InterruptedException {
        testGenderDetectorEndpoint(FIRST_GENDER_DETECTOR, "Jan Maria Rokita", Gender.MALE);
    }

    @Test
    void shouldGetInonclusiveForNotFoundNamesVariantFirst() throws IOException, InterruptedException {
        testGenderDetectorEndpoint(FIRST_GENDER_DETECTOR, "AAAEEEOOIAAAAA", Gender.INCONCLUSIVE);
    }

    @Test
    void shouldGetFemaleForMajorFemaleNameWithVariantMajor() throws IOException, InterruptedException {
        testGenderDetectorEndpoint(MAJOR_GENDER_DETECTOR, "Jan Maria Anna", Gender.FEMALE);
    }

    @Test
    void shouldGetMaleForMajorMaleNameWithVariantMajor() throws IOException, InterruptedException {
        testGenderDetectorEndpoint(MAJOR_GENDER_DETECTOR, "Jan Maria Sebastian", Gender.MALE);
    }

    @Test
    void shouldGetInonclusiveForNotFoundNamesVariantMajor() throws IOException, InterruptedException {
        testGenderDetectorEndpoint(MAJOR_GENDER_DETECTOR, "AAAEEEOOIAAAAA", Gender.INCONCLUSIVE);
    }

    @Test
    void shouldGetInonclusiveFor1Male1FemaleNamesVariantMajor() throws IOException, InterruptedException {
        testGenderDetectorEndpoint(MAJOR_GENDER_DETECTOR, "Jan Maria Rokita", Gender.INCONCLUSIVE);
    }

    @Test
    void shouldGetInonclusiveForCommonNameInMalesAndFemalesList() throws IOException, InterruptedException {
        testGenderDetectorEndpoint(MAJOR_GENDER_DETECTOR, "common", Gender.INCONCLUSIVE);
    }

    private void testGenderDetectorEndpoint(GenderDetectorVariant detectorVariant, String givenNames, Gender expectedGender)
            throws IOException, InterruptedException {
        // given
        String url = getUrl(givenNames, detectorVariant);
        var uri = URI.create(url);
        var req = HttpRequest.newBuilder()
                .uri(uri)
                .GET()
                .build();
        var respBodyHandler = HttpResponse.BodyHandlers.ofString();

        // when
        HttpResponse<String> resp = httpClient.send(req, respBodyHandler);

        // then
        assertEquals(expectedGender.getValue(), resp.body());
    }

    private String getUrl(String names, GenderDetectorVariant variant) {
        String encodedNames = URLEncoder.encode(names, StandardCharsets.UTF_8);
        return new StringBuilder()
                .append("http://localhost:")
                .append(serverPort)
                .append("/v1/gender/names/")
                .append(encodedNames)
                .append("?variant=")
                .append(variant.getType().getValue())
                .append("&version=")
                .append(variant.getVersion())
                .toString();
    }
}
